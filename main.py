import RPi.GPIO as GPIO
import time
import signal
import sys
import os

GPIO.setmode(GPIO.BCM)
pin = 4
GPIO.setup(pin, GPIO.IN)

#questo file (se presente) viene usato per accendere la luce
interr_path= '/home/fox/test-python/relay/lamp/.on'    


#toggle the light switch
def toggle(pin):
    if not os.path.isfile(interr_path):
        file = open(interr_path, "w")
        file.write ("1")
        file.close()
    time.sleep(10)
    os.remove(interr_path)

def handler (signal, frame):
    print ("Closing... Bye!!")
    GPIO.cleanup()
    sys.exit(0)

#if the sensor finds someone the function calls toggle
GPIO.add_event_detect(pin, GPIO.RISING, callback=toggle)
#if ctrl+c pressed close the program
signal.signal (signal.SIGINT, handler)

while (1):
    time.sleep(5)
    
